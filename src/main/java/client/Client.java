package client;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by ljozwiak on 08/05/2015.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "client", namespace = "http://ljozwiak.pl/client")
public class Client {

    @XmlElement(name = "first_name")
    private String firstName;

    private String surname;

    @XmlAttribute
    private int id;
    @XmlTransient
    private BigInteger internaleId;
    @XmlElement(name = "birth_date")
    private Date birthDate;
    private Set<Account> accounts;

    public BigInteger getInternaleId() {
        return internaleId;
    }

    public void setInternaleId(BigInteger internaleId) {
        this.internaleId = internaleId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Account> getAccounts() {
        if (accounts == null) {
            accounts = new HashSet<Account>();
        }
        return accounts;
    }

    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
