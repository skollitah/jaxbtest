package client;

import java.math.BigDecimal;

import com.google.common.base.Objects;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class Account {

    private String number;

    private BigDecimal balance;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(number, balance);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != Account.class) {
            return false;
        } else {
            final Account other = (Account) obj;
            return Objects.equal(this.number, other.number) && Objects.equal(this.balance, other.balance);
        }
    }
}
