package process;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public final class JaxbUtlis {

    private static Logger logger = Logger.getLogger(JaxbUtlis.class);

    public static <T> T unmarshal(final String xml, Class<T> clazz) {
        T result = null;

        try {
            final JAXBContext context = JAXBContext.newInstance(clazz);
            final Unmarshaller unmarshaller = context.createUnmarshaller();
            final StringReader stringReader = new StringReader(xml);
            result = (T) unmarshaller.unmarshal(stringReader);
        } catch (JAXBException e) {
            logger.error(e);
        }

        return result;
    }

    public static <T> String marshall(final T object, Class<T> clazz) {
        String result = "";

        try {
            final JAXBContext context = JAXBContext.newInstance(clazz);
            final Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            final StringWriter stringWriter = new StringWriter();
            marshaller.marshal(object, stringWriter);
            result = stringWriter.toString();
        } catch (JAXBException e) {
            logger.error(e);
        }

        return result;
    }

}
