package client;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.joda.time.LocalDate;
import org.junit.Test;

import process.JaxbUtlis;

import com.google.common.collect.Sets;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class ClientSerializationTest {

    private final static String FIRST_NAME = "Luke";
    private final static String SURNAME = "Skywalker";
    private final static Date BIRTH_DATE = new LocalDate(2000, 2, 12).toDate();
    private final static int ID = 10;
    private final static BigInteger INTERNAL_ID = BigInteger.valueOf(10L);
    private final static String ACCOUNT_NUMBER_1 = "XX100";
    private final static BigDecimal ACCOUNT_BALANCE_1 = new BigDecimal("100.00");
    private final static String ACCOUNT_NUMBER_2 = "XX200";
    private final static BigDecimal ACCOUNT_BALANCE_2 = new BigDecimal("200.00");

    @Test
    public void shouldSerializeAndDeserializeClient() {
        // given
        final Client client = new Client();
        client.setId(ID);
        client.setInternaleId(INTERNAL_ID);
        client.setBirthDate(BIRTH_DATE);
        client.setFirstName(FIRST_NAME);
        client.setSurname(SURNAME);

        final Account account1 = new Account();
        account1.setNumber(ACCOUNT_NUMBER_1);
        account1.setBalance(ACCOUNT_BALANCE_1);

        final Account account2 = new Account();
        account2.setNumber(ACCOUNT_NUMBER_2);
        account2.setBalance(ACCOUNT_BALANCE_2);

        client.setAccounts(Sets.newHashSet(account1, account2));

        // when
        final String xml = JaxbUtlis.marshall(client, Client.class);
        final Client result = JaxbUtlis.unmarshal(xml, Client.class);

        // then
        assertThat(result).isNotNull();
        assertThat(result.getId()).isEqualTo(ID);
        assertThat(result.getInternaleId()).isNull(); // because it is transient
        assertThat(result.getBirthDate()).isEqualTo(BIRTH_DATE);
        assertThat(result.getFirstName()).isEqualTo(FIRST_NAME);
        assertThat(result.getSurname()).isEqualTo(SURNAME);
        assertThat(result.getAccounts()).containsOnly(account1, account2);
    }
}
